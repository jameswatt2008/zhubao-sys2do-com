# -*- coding: utf-8 -*-

import sys
import traceback
from datetime import datetime as dt


from sys2do.model import DBSession, metadata, engine
from sys2do.model.auth import User, Group, Permission
from sys2do.model.master import Customer, Shop, Supplier, Product, ShopProfile, \
    ProductType, InventoryLocation, Paytype, Payterm, Currency, FinAccount, \
    ProductSeries, ProductShap, CurrencyRatio
from sys2do.constant import SHOP_SALE_TYPE_SELF, SHOP_SALE_TYPE_JOIN, \
    SHOP_TYPE_MAIN, SHOP_TYPE_BRANCH
from sys2do.model.system import SysDict
reload( sys )
sys.setdefaultencoding( 'utf8' )

def init():
    try:
        print "create tables"
        metadata.drop_all( engine )
        metadata.create_all( engine )
        print 'create user'
        uAdmin = User( name = 'admin', email = 'admin@sys2do.com', password = 'admin' )
        uTest1 = User( name = 'test1', email = 'test@sys2do.com', password = 'test' )
        uTest2 = User( name = 'test2', email = 'test@sys2do.com', password = 'test' )

        print 'create grpup'
        gAdmin = Group( name = 'ADMINGRP' )
        gAdmin.users = [uAdmin]
        gSZ = Group( name = 'SZGRP' )
        gSZ.users = [uTest1]
        gCS = Group( name = 'CSGRP' )
        gCS.users = [uTest2]

        print 'create permission'
        pSU = Permission( name = 'SUPER_USER', desc = u'超级管理员，无限制任何操作。' )

        pUsrView = Permission( name = 'USER_VIEW', desc = u'是否可以查看账号信息' )
        pUsrCreate = Permission( name = 'USER_CREATE', desc = u'是否可以新建账号' )
        pUsrEdit = Permission( name = 'USER_EDIT', desc = u'是否可以编辑账号资料' )
        pUsrDel = Permission( name = 'USER_DEL', desc = u'是否可以删除账号' )

        pGrpView = Permission( name = 'GROUP_VIEW', desc = u'是否可以新建组别' )
        pGrpCreate = Permission( name = 'GROUP_CREATE', desc = u'是否可以新建组别' )
        pGrpEdit = Permission( name = 'GROUP_EDIT', desc = u'是否可以编辑组别资料' )
        pGrpDel = Permission( name = 'GROUP_DEL', desc = u'是否可以删除组别' )

        pMasterCreate = Permission( name = 'MASTER_CREATE', desc = u'是否可以新建基本属性' )
        pMasterEdit = Permission( name = 'MASTER_EDIT', desc = u'是否可以编辑基本属性' )
        pMasterDel = Permission( name = 'MASTER_DEL', desc = u'是否可以删除基本属性' )

        pPdtViewAll = Permission( name = 'PRODUCT_VIEW_ALL', desc = u'是否可以查看所有商品' )
        pPdtView = Permission( name = 'PRODUCT_VIEW', desc = u'是否可以查看商品' )
        pPdtCreate = Permission( name = 'PRODUCT_CREATE', desc = u'是否可以新建商品' )
        pPdtEdit = Permission( name = 'PRODUCT_EDIT', desc = u'是否可以编辑商品信息' )
        pPdtDel = Permission( name = 'PRODUCT_DEL', desc = u'是否可以删除商品' )

        pSpViewAll = Permission( name = 'SHOP_VIEW_ALL', desc = u'是否可以查看全部店铺信息' )
        pSpView = Permission( name = 'SHOP_VIEW', desc = u'是否可以查看店铺信息' )
        pSpCreate = Permission( name = 'SHOP_CREATE', desc = u'是否可以新建店铺' )
        pSpEdit = Permission( name = 'SHOP_EDIT', desc = u'是否可以编辑店铺信息' )
        pSpDel = Permission( name = 'SHOP_DEL', desc = u'是否可以删除店铺' )

        pMemView = Permission( name = 'MEMBER_VIEW', desc = u'是否可以查看会员信息' )
        pMemCreate = Permission( name = 'MEMBER_CREATE', desc = u'是否可以新建会员' )
        pMemEdit = Permission( name = 'MEMBER_EDIT', desc = u'是否可以编辑会员信息' )
        pMemDel = Permission( name = 'MEMBER_DEL', desc = u'是否可以删除会员' )

        pIvtView = Permission( name = 'INVENTORY_VIEW', desc = u'是否可以查看仓位信息' )
        pIvtCreate = Permission( name = 'INVENTORY_CREATE', desc = u'是否可以新建仓位' )
        pIvtEdit = Permission( name = 'INVENTORY_EDIT', desc = u'是否可以编辑仓位信息' )
        pIvtDel = Permission( name = 'INVENTORY_DEL', desc = u'是否可以删除仓位' )

#         pIvtPdtEdit = Permission( name = 'INVENTORY_PRODUCT_EDIT', desc = u'是否可以编辑仓库中商品数量' )


        pPrhView = Permission( name = 'PURCHASE_VIEW', desc = u'是否可以查看采购单' )
        pPrhCreate = Permission( name = 'PURCHASE_CREATE', desc = u'是否可以新建采购单' )
        pPrhEdit = Permission( name = 'PURCHASE_EDIT', desc = u'是否可以编辑采购单信息' )
        pPrhDel = Permission( name = 'PURCHASE_DEL', desc = u'是否可以删除采购单' )
        pPrhApp = Permission( name = 'PURCHASE_APPROVE', desc = u'是否可以审批采购单' )

        pSplView = Permission( name = 'SUPPLIER_VIEW', desc = u'是否可以查看供应商信息' )
        pSplCreate = Permission( name = 'SUPPLIER_CREATE', desc = u'是否可以新建供应商' )
        pSplEdit = Permission( name = 'SUPPLIER_EDIT', desc = u'是否可以编辑供应商资料' )
        pSplDel = Permission( name = 'SUPPLIER_DEL', desc = u'是否可以删除供应商' )
        pSplApp = Permission( name = 'SUPPLIER_APPROVE', desc = u'是否可以审批供应商资料' )

        pSleView = Permission( name = 'SALE_VIEW', desc = u'是否可以查看销售单' )
        pSleCreate = Permission( name = 'SALE_CREATE', desc = u'是否可以新建销售单' )
        pSleEdit = Permission( name = 'SALE_EDIT', desc = u'是否可以编辑销售单信息' )
        pSleDel = Permission( name = 'SALE_DEL', desc = u'是否可以删除销售单' )
        pSleApp = Permission( name = 'SALE_APPROVE', desc = u'是否可以审批销售单' )

        pDNView = Permission( name = 'DN_VIEW', desc = u'是否可以查看调拨单' )
        pDNCreate = Permission( name = 'DN_CREATE', desc = u'是否可以新建调拨单' )
        pDNEdit = Permission( name = 'DN_EDIT', desc = u'是否可以编辑调拨单信息' )
        pDNDel = Permission( name = 'DN_DEL', desc = u'是否可以删除调拨单' )
        pDNApp = Permission( name = 'DN_APPROVE', desc = u'是否可以审批调拨单' )

        pRGNView = Permission( name = 'RGN_VIEW', desc = u'是否可以查看退货单' )
        pRGNCreate = Permission( name = 'RGN_CREATE', desc = u'是否可以新建退货单' )
        pRGNEdit = Permission( name = 'RGN_EDIT', desc = u'是否可以编辑退货单信息' )
        pRGNDel = Permission( name = 'RGN_DEL', desc = u'是否可以删除退货单' )
        pRGNApp = Permission( name = 'RGN_APPROVE', desc = u'是否可以审批退货单' )

        pIvtntView = Permission( name = 'INVENTORY_NOTE_VIEW', desc = u'是否可以查看出入库单' )
        pIvtntCreate = Permission( name = 'INVENTORY_NOTE_CREATE', desc = u'是否可以新建出入库单' )
        pIvtntEdit = Permission( name = 'INVENTORY_NOTE_EDIT', desc = u'是否可以编辑入库单信息' )
        pIvtntDel = Permission( name = 'INVENTORY_NOTE_DEL', desc = u'是否可以删除入库单' )
        pIvtntApp = Permission( name = 'INVENTORY_NOTE_APPROVE', desc = u'是否可以审批入库单' )

        pFinViewAllIN = Permission( name = 'FIN_VIEW_ALL_IN', desc = u'是否可以查看所有商铺的应收财务状况' )
        pFinViewIN = Permission( name = 'FIN_IN_VIEW', desc = u'是否可以查看商铺的应收财务状况' )
        pFinCreateIN = Permission( name = 'FIN_IN_CREATE', desc = u'是否可以新建应收账单' )
        pFinEditIN = Permission( name = 'FIN_IN_EDIT', desc = u'是否可以编辑应收账单信息' )
        pFinDelIN = Permission( name = 'FIN_IN_DEL', desc = u'是否可以删除应收账单' )
        pFinINApp = Permission( name = 'FIN_IN_APPROVE', desc = u'是否可以审批应收账单' )

        pFinViewAllOUT = Permission( name = 'FIN_VIEW_ALL_OUT', desc = u'是否可以查看所有商铺的应付财务状况' )
        pFinViewOUT = Permission( name = 'FIN_OUT_VIEW', desc = u'是否可以查看商铺的应付财务状况' )
        pFinCreateOUT = Permission( name = 'FIN_OUT_CREATE', desc = u'是否可以新建应付账单' )
        pFinEditOUT = Permission( name = 'FIN_OUT_EDIT', desc = u'是否可以编辑应付账单信息' )
        pFinDelOUT = Permission( name = 'FIN_OUT_DEL', desc = u'是否可以删除应付账单' )
        pFinOutApp = Permission( name = 'FIN_OUT_APPROVE', desc = u'是否可以审批应付账单' )

        pFinAccCreate = Permission( name = 'FIN_ACCOUNT_CREATE', desc = u'是否可以新建账户' )
        pFinAccEdit = Permission( name = 'FIN_ACCOUNT_EDIT', desc = u'是否可以编辑账户信息' )
        pFinAccDel = Permission( name = 'FIN_ACCOUNT_DEL', desc = u'是否可以删除账户' )

        allPrms = [pSU,
                   pUsrView, pUsrCreate, pUsrEdit, pUsrDel,
                   pGrpView, pGrpCreate, pGrpEdit, pGrpDel,
                   pMasterCreate, pMasterEdit, pMasterDel,
                   pPdtViewAll, pPdtView, pPdtCreate, pPdtEdit, pPdtDel,
                   pSpView, pSpCreate, pSpEdit, pSpDel, pSpViewAll,
                   pMemView, pMemCreate, pMemEdit, pMemDel,
                   pIvtView, pIvtCreate, pIvtEdit, pIvtDel,
                   pPrhView, pPrhCreate, pPrhEdit, pPrhDel, pPrhApp,
                   pSplView, pSplCreate, pSplEdit, pSplDel, pSplApp,
                   pDNView, pDNCreate, pDNEdit, pDNDel, pDNApp,
                   pRGNView, pRGNCreate, pRGNEdit, pRGNDel, pRGNApp,
                   pSleView, pSleCreate, pSleEdit, pSleDel, pSleApp, pIvtntView, pIvtntCreate, pIvtntEdit, pIvtntDel, pIvtntApp,
                   pFinViewAllIN, pFinViewIN, pFinCreateIN, pFinEditIN, pFinDelIN, pFinINApp,
                   pFinViewAllOUT, pFinViewOUT, pFinCreateOUT, pFinEditOUT, pFinDelOUT, pFinOutApp, pFinAccCreate, pFinAccEdit, pFinAccDel ]

        gAdmin.permissions = allPrms
        gSZ.permissions = [
                           pMasterCreate, pMasterEdit, pMasterDel,
                           pPdtViewAll, pPdtView, pPdtCreate, pPdtEdit, pPdtDel,
                           pSpView, pSpCreate, pSpEdit, pSpDel, pSpViewAll,
                           pMemView, pMemCreate, pMemEdit, pMemDel,
                           pIvtView, pIvtCreate, pIvtEdit, pIvtDel,
                           pPrhView, pPrhCreate, pPrhEdit, pPrhDel, pPrhApp,
                           pSplView, pSplCreate, pSplEdit, pSplDel, pSplApp,
                           pDNView, pDNCreate, pDNEdit, pDNDel, pDNApp,
                           pRGNView, pRGNCreate, pRGNEdit, pRGNDel, pRGNApp,
                           pSleView, pSleCreate, pSleEdit, pSleDel, pSleApp, pIvtntView, pIvtntCreate, pIvtntEdit, pIvtntDel, pIvtntApp,
                           pFinViewAllIN, pFinViewIN, pFinCreateIN, pFinEditIN, pFinDelIN, pFinINApp,
                           pFinViewAllOUT, pFinViewOUT, pFinCreateOUT, pFinEditOUT, pFinDelOUT, pFinOutApp, pFinAccCreate, pFinAccEdit, pFinAccDel
                           ]
        gCS.permissions = [
                           pMemView, pMemCreate, pMemEdit, pMemDel,
                           pIvtView, pIvtCreate, pIvtEdit, pIvtDel,
                           pSplView, pSplCreate, pSplEdit, pSplDel, pSplApp,
                           pDNView, pDNCreate, pDNEdit, pDNDel, pDNApp,
                           pRGNView, pRGNCreate, pRGNEdit, pRGNDel, pRGNApp,
                           pSleView, pSleCreate, pSleEdit, pSleDel, pSleApp, pIvtntView, pIvtntCreate, pIvtntEdit, pIvtntDel, pIvtntApp,
                           pFinViewAllIN, pFinViewIN, pFinCreateIN, pFinEditIN, pFinDelIN, pFinINApp,
                           pFinViewAllOUT, pFinViewOUT, pFinCreateOUT, pFinEditOUT, pFinDelOUT, pFinOutApp, pFinAccCreate, pFinAccEdit, pFinAccDel
                           ]

        print 'create shop'
        sp1 = Shop( shortName = '旗舰店', name = '广东省深圳市南山区分店', fund = u'300万', legalPerson = u'罗小姐' )
        sp2 = Shop( shortName = '长沙店', name = '湖南省长沙市分店', fund = u'200万', legalPerson = u'周先生' )

        cst1 = Customer( name = u'客户一', address = u"深圳", createTime = dt.now() )
        cst2 = Customer( name = u'客户二', address = u"东莞", createTime = dt.now() )

        spl1 = Supplier( name = '玉华', fullName = u'惠州玉华有限公司', address = u'惠州' , att = u'李先生', tel = '0752-25316060', mobile = '13800138111' )
        spl2 = Supplier( name = '美钻', fullName = u'汕尾美钻饰品有限公司', address = u'汕尾' , att = u'张小姐', tel = '0660-87456012', mobile = '13800138222' )

        print 'create product'
        pdttp1 = ProductType( name = u'钻石', createTime = dt.now(), updateTime = dt.now() )
        pdttp2 = ProductType( name = u'铂金', createTime = dt.now(), updateTime = dt.now() )
        pdttp3 = ProductType( name = u'翡翠', createTime = dt.now(), updateTime = dt.now() )
        pdttp4 = ProductType( name = u'K金', createTime = dt.now(), updateTime = dt.now() )
        pdttp5 = ProductType( name = u'宝石', createTime = dt.now(), updateTime = dt.now() )
        pdttp6 = ProductType( name = u'珍珠', createTime = dt.now(), updateTime = dt.now() )
        pdttp7 = ProductType( name = u'黄金', createTime = dt.now(), updateTime = dt.now() )
        pdttp8 = ProductType( name = u'银类', createTime = dt.now(), updateTime = dt.now() )


#         pdt1 = Product( name = u'黄金一', desc = u'黄金', type = pdttp1, pPrice = 1000, sPrice = 3000 )
#         pdt1.assignNo()
#         pdt2 = Product( name = u'钻石一', desc = u'钻石', type = pdttp2, pPrice = 1200, sPrice = 3600 )
#         pdt2.assignNo()

        print 'create master'
        ptyp1 = Paytype( name = u'月结', createTime = dt.now() )
        ptyp2 = Paytype( name = u'预付', createTime = dt.now() )
        ptyp3 = Paytype( name = u'现付', isDefault = 1, createTime = dt.now() )

        ptm1 = Payterm( name = u'现金', isDefault = 1, createTime = dt.now() )
        ptm2 = Payterm( name = u'借读卡', createTime = dt.now() )
        ptm3 = Payterm( name = u'储蓄卡', createTime = dt.now() )

        rmb = Currency( code = 'RMB', name = u'人民币', isDefault = 1, ratio = 1, createTime = dt.now(), updateTime = dt.now() )
        rmbv1 = CurrencyRatio( currency = rmb, ratio = 1, effTime = dt.now() )
        hkd = Currency( code = 'HKD', name = u'港币', ratio = 0.79, createTime = dt.now(), updateTime = dt.now() )
        hkdv1 = CurrencyRatio( currency = hkd, ratio = 0.79, effTime = dt.now() )
        usd = Currency( code = 'USD', name = u'美金', ratio = 6.14, createTime = dt.now(), updateTime = dt.now() )
        usdv1 = CurrencyRatio( currency = usd, ratio = 6.14, effTime = dt.now() )


        psr1 = ProductSeries( name = u'吉祥', createTime = dt.now() )
        psr2 = ProductSeries( name = u'婚庆', createTime = dt.now() )
        psr3 = ProductSeries( name = u'爱情', createTime = dt.now() )

        psap1 = ProductShap( name = u'圆形', createTime = dt.now() )
        psap2 = ProductShap( name = u'方形', createTime = dt.now() )
        psap3 = ProductShap( name = u'三角形', createTime = dt.now() )
        psap4 = ProductShap( name = u'椭圆形', createTime = dt.now() )

        DBSession.add_all( [uAdmin, uTest1, uTest2, gSZ, gCS,
                           pUsrView, pUsrCreate, pUsrEdit, pUsrDel,
                           pGrpView, pGrpCreate, pGrpEdit, pGrpDel,
                           pMasterCreate, pMasterEdit, pMasterDel,
                           pPdtViewAll, pPdtView, pPdtCreate, pPdtEdit, pPdtDel,
                           pSpView, pSpCreate, pSpEdit, pSpDel,
                           pMemView, pMemCreate, pMemEdit, pMemDel,
                           pIvtView, pIvtCreate, pIvtEdit, pIvtDel,
                           pPrhView, pPrhCreate, pPrhEdit, pPrhDel, pPrhApp,
                           pSplView, pSplCreate, pSplEdit, pSplDel, pSplApp,
                           pDNView, pDNCreate, pDNEdit, pDNDel, pDNApp,
                           pRGNView, pRGNCreate, pRGNEdit, pRGNDel, pRGNApp,
                           pSleView, pSleCreate, pSleEdit, pSleDel, pSleApp, pIvtntView, pIvtntCreate, pIvtntEdit, pIvtntDel, pIvtntApp,
                           pFinViewAllIN, pFinViewIN, pFinCreateIN, pFinEditIN, pFinDelIN, pFinINApp,
                           pFinViewAllOUT, pFinViewOUT, pFinCreateOUT, pFinEditOUT, pFinDelOUT, pFinOutApp,
                           pFinAccCreate, pFinAccEdit, pFinAccDel,
                           sp1, sp2, cst1, cst2,
                           spl1, spl2, pdttp1, pdttp2, pdttp3, pdttp4, pdttp5, pdttp6, pdttp7, pdttp8,
#                             pdt1, pdt2,
                           ptyp1, ptyp2, ptyp3,
                           ptm1, ptm2, ptm3, rmb, rmbv1, hkd, hkdv1, usd, usdv1,
                           psr1, psr2, psr3, psap1, psap2, psap3, psap4,
                           ] )
        DBSession.flush()
        ivttravel = InventoryLocation( name = u'IN_TRAVEL', address = None, fullPath = '在途', referID = None, type = 0 )    # 在途
        ivtprh = InventoryLocation( name = u'PURCHASE', address = None, fullPath = '采购', referID = None, type = 0 )    # 采购
        ivtsoldout = InventoryLocation( name = u'SOLD_OUT', address = None, fullPath = '销售', referID = None, type = 0 )    # 售出
        ivtwastage = InventoryLocation( name = u'WASTAGE', address = None, fullPath = '损耗', referID = None, type = 0 )    # 损耗
        ivtprocess = InventoryLocation( name = u'PROCESS', address = None, fullPath = '加上/维修', referID = None, type = 0 )    # 加工

        ivt1 = InventoryLocation( name = u'旗舰店仓库', address = None, fullPath = '旗舰店仓库', referID = sp1.id, type = 0, lft = 1, rgt = 6 )
        ivt11 = InventoryLocation( parent = ivt1, name = u'A区', address = u'旗舰店仓库A区', fullPath = '旗舰店仓库A区', referID = sp1.id, type = 1, lft = 2, rgt = 5 )
        ivt111 = InventoryLocation( parent = ivt11, name = u'B架', address = u'旗舰店仓库A区B架', fullPath = '旗舰店仓库A区B架', referID = sp1.id, type = 1, lft = 3, rgt = 4 )
        ivt2 = InventoryLocation( name = u'长沙店仓库', address = None, fullPath = '长沙店仓库', referID = sp2.id, type = 0, lft = 7, rgt = 8 )

        pro1 = ShopProfile( shop = sp1, groupIDs = [gSZ.id], inventory = ivt1 , saleType = SHOP_SALE_TYPE_SELF, shopType = SHOP_TYPE_MAIN )
        pro2 = ShopProfile( shop = sp2, groupIDs = [gCS.id], inventory = ivt2 , saleType = SHOP_SALE_TYPE_JOIN, shopType = SHOP_TYPE_BRANCH )

        acc1 = FinAccount( name = u'旗舰店账户', shopProfile = pro1, currency = rmb, code = '1111222233334444', owner = '罗小姐', paytype = None, amount = 0, )
        acc2 = FinAccount( name = u'长沙店账户', shopProfile = pro2, currency = rmb, code = '2222333344445555', owner = '周先生', paytype = None, amount = 0, )
        DBSession.add_all( [ivttravel, ivtprh, ivtsoldout, ivtwastage, ivtprocess, ivt1, ivt11, ivt111, ivt2, pro1, pro2,
                            acc1, acc2
                            ] )

        print 'create syste dict'
        DBSession.add_all( [
                           SysDict( type = 'OBJECT_PREFIX', name = 'Payterm', value = '010', remark = u'结算方式号码编码前缀' ),
                           SysDict( type = 'OBJECT_PREFIX', name = 'Paytype', value = '011', remark = u'支付方式号码编码前缀' ),
                           SysDict( type = 'OBJECT_PREFIX', name = 'InventoryLocation', value = '012', remark = u'仓位号码编码前缀' ),
                           SysDict( type = 'OBJECT_PREFIX', name = 'Supplier', value = '013', remark = u'供应商号码编码前缀' ),
                           SysDict( type = 'OBJECT_PREFIX', name = 'Customer', value = '014', remark = u'客户号码编码前缀' ),
                           SysDict( type = 'OBJECT_PREFIX', name = 'ProductType', value = '015', remark = u'类型号码编码前缀' ),
                           SysDict( type = 'OBJECT_PREFIX', name = 'ProductShap', value = '016', remark = u'形状号码编码前缀' ),
                           SysDict( type = 'OBJECT_PREFIX', name = 'ProductSeries', value = '017', remark = u'系列号码编码前缀' ),
                           SysDict( type = 'OBJECT_PREFIX', name = 'Shop', value = '018', remark = u'店铺号码编码前缀' ),
                           SysDict( type = 'OBJECT_PREFIX', name = 'ShopProfile', value = '019', remark = u'店铺配置号码编码前缀' ),
                           SysDict( type = 'OBJECT_PREFIX', name = 'Currency', value = '020', remark = u'汇率号码编码前缀' ),
                           SysDict( type = 'OBJECT_PREFIX', name = 'FinAccount', value = '021', remark = u'账户号码编码前缀' ),
                           SysDict( type = 'OBJECT_PREFIX', name = 'Member', value = '111', remark = u'会员号码编码前缀' ),
                           SysDict( type = 'OBJECT_PREFIX', name = 'Product', value = '100', remark = u'商品号码编码前缀' ),
                           SysDict( type = 'OBJECT_PREFIX', name = 'SO', value = '200', remark = u'销售单号编码前缀' ),
                           SysDict( type = 'OBJECT_PREFIX', name = 'PO', value = '300', remark = u'采购单号编码前缀' ),
                           SysDict( type = 'OBJECT_PREFIX', name = 'DN', value = '400', remark = u'送货单编码前缀' ),
                           SysDict( type = 'OBJECT_PREFIX', name = 'RGN', value = '500', remark = u'退货单号编码前缀' ),
                           SysDict( type = 'OBJECT_PREFIX', name = 'InventoryNote', value = '600', remark = u'出入库单号编码前缀' ),
                           SysDict( type = 'OBJECT_PREFIX', name = 'FinNote', value = '700', remark = u'进出账单号编码前缀' ),
                           ] )


        DBSession.flush()

        ivttravel.fullPathIDs = ivttravel.id
        ivtsoldout.fullPathIDs = ivtsoldout.id
        ivtprh.fullPathIDs = ivtprh.id
        ivtprocess.fullPathIDs = ivtprocess.id
        ivtwastage.fullPathIDs = ivtwastage.id
        ivt1.fullPathIDs = ivt1.id
        ivt11.fullPathIDs = '|'.join( map( unicode, [ivt1.id, ivt11.id] ) )
        ivt111.fullPathIDs = '|'.join( map( unicode, [ivt1.id, ivt11.id, ivt111.id] ) )
        ivt2.fullPathIDs = ivt2.id


        print 'insert privine, city, bank'
        province_f = open( 'sys2do/sql/master_province.sql' )
        province_sql = "".join( province_f.readlines() )
        province_f.close()

        city_f = open( 'sys2do/sql/master_city.sql' )
        city_sql = "".join( city_f.readlines() )
        city_f.close()

        bank_f = open( 'sys2do/sql/master_bank.sql' )
        bank_sql = "".join( bank_f.readlines() )
        bank_f.close()

        conn = DBSession.connection()
        conn.execute( province_sql )
        conn.execute( city_sql )
        conn.execute( bank_sql )

        DBSession.commit()
        print "finish init!"
    except:
        traceback.print_exc()
        DBSession.rollback()

if __name__ == '__main__':
    init()
