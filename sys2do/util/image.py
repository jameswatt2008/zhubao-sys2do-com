#coding=utf-8
import Image, traceback
from decimal import Decimal

def getPixels(filepath):
    width, height = Image.open(filepath).size
    return [width, height]

def getThumbnailSize(img_path, org_width, org_height):
    if img_path:
        try:
            pixels = getPixels(img_path)
            widthPct = Decimal(pixels[0])/org_width
            heightPct = Decimal(pixels[1])/org_height
            _colWidth, _colHeight = None, None
            _marWidth, _marHeight = 0, 0
            if(heightPct>widthPct):
                _colWidth = round(Decimal(pixels[0])/heightPct)
                _colHeight = org_height
                _marWidth = (org_width-_colWidth)/2
            else:
                _colWidth = org_width
                _colHeight = round(Decimal(pixels[1])/widthPct)
                _marHeight = (org_height-_colHeight)/2
            return [_colWidth, _colHeight, _marWidth, _marHeight]
        except IOError, e:
            pass
        except Exception, e:
            traceback.print_exc()
            return None
    else:
        return None
