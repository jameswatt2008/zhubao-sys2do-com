# -*- coding: utf-8 -*-
'''
Created on 2013-7-11
@author: CL.lam
'''

from sys2do.constant import MSG_MORE_THAN_STOCK, MSG_NO_INVENTORY_QTY, \
    MSG_NO_INVENTORY_PROVIDED, MSG_NO_RELATED_ITEM, MSG_ITEM_QTY_NOT_MATCH, \
    MSG_PRODUCT_ITEM_NOT_MATCH, MSG_ITEM_NO_IN_CORRECT_INVENTORY, MSG_ITEM_ALREADY_OCCUPIED, \
    MSG_NOT_ALL_PARAMS_OK

__all__ = ['CustomizedExp',
           'ParamsNotOKExp',
           'MoreThanStockExp', 'NoInventoryExp', 'NoInventoryProvidedExp',
           'NoRelatedItemExp', 'ItemQtyNotMatchExp', 'ProdutItemNotMatchExp',
           'ItemNoInCorrectInventoryExp', 'ItemAlreadyoccupiedExp']


class CustomizedExp( Exception ):
    msg = None

    def __str__( self ): return self.msg
    def __repr__( self ): return self.msg
    def __unicode__( self ): return self.msg


class ParamsNotOKExp( CustomizedExp ):
    msg = MSG_NOT_ALL_PARAMS_OK


class MoreThanStockExp( CustomizedExp ):
    msg = MSG_MORE_THAN_STOCK


class NoInventoryExp( CustomizedExp ):
    msg = MSG_NO_INVENTORY_QTY


class NoInventoryProvidedExp( CustomizedExp ):
    msg = MSG_NO_INVENTORY_PROVIDED


class NoRelatedItemExp( CustomizedExp ):
    msg = MSG_NO_RELATED_ITEM


class ItemQtyNotMatchExp( CustomizedExp ):
    msg = MSG_ITEM_QTY_NOT_MATCH


class ProdutItemNotMatchExp( CustomizedExp ):
    msg = MSG_PRODUCT_ITEM_NOT_MATCH


class ItemNoInCorrectInventoryExp( CustomizedExp ):
    msg = MSG_ITEM_NO_IN_CORRECT_INVENTORY


class ItemAlreadyoccupiedExp( CustomizedExp ):
    msg = MSG_ITEM_ALREADY_OCCUPIED
