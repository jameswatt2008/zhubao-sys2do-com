# -*- coding: utf-8 -*-

import datetime
from jinja2.filters import do_default
from sys2do.constant import *

__all__ = ['ft', 'fd', 'f', 'ss']


def ft( t, f = SYSTEM_DATETIME_FORMAT ):
    try:
        return t.strftime( f )
    except:
        return '' if not t else str( t )


def fd( d, f = SYSTEM_DATE_FORMAT ):
    try:
        return d.strftime( f )
    except:
        return '' if not d else str( d )


f = lambda v, default = u'' : do_default( v, default ) or default



def ss( v, c ):
    if c == 'PO':
        if v == PO_NEW : return u'新建'
        if v == PO_APPROVE : return u'批准'
    elif c == 'SO':
        if v == SO_NEW : return u'新建'
        if v == SO_APPROVE : return u'批准'
    elif c == 'IVTNT':
        if v == IVTNT_IN : return u'入库'
        if v == IVTNT_OUT : return u'出库'
        if v == IVTNT_INTERNAL : return u'内部转仓'
        if v == IVTNT_NEW : return u'新建'
        if v == IVTNT_APPROVE : return u'批准'
        if v == IVTNT_DISAPPROVE : return u'拒绝'
        if v == IVTNT_TO_WASTAGE : return u'损耗'
    if c == 'TNS' :
        if v == TNS_NEW : return u'新建'
        if v == TNS_SND_AP : return u'批准'
        if v == TNS_TRAVEL : return u'在途'
        if v == TNS_RCV : return u'已收货'
    if c == 'RG' :
        if v == RGN_NEW : return u'新建'
        if v == RGN_APPROVE : return u'批准'
        if v == RGN_REJECT : return u'拒绝'
    if c == 'FIN':
        if v == FIN_NEW : return u'新建'
        if v == FIN_APPROVE : return u'批准'
        if v == FIN_REJECT : return u'拒绝'
        if v == FIN_IN : return u'应收'
        if v == FIN_OUT : return u'应付'
    if c == 'SPTP':
        if v == SHOP_TYPE_MAIN : return u'总店'
        if v == SHOP_TYPE_BRANCH : return u'门店'
    if c == 'SPSLTP':
        if v == SHOP_SALE_TYPE_SELF : return u'自营店'
        if v == SHOP_SALE_TYPE_JOIN : return u'加盟店'
    if c == 'IVTNAME':
        if v == 'IN_TRAVEL' :return u'在途'
        if v == 'SOLD_OUT' :return u'售出'
        if v == 'WASTAGE' : return u'损耗'
        if v == 'PURCHASE' : return u'采购'
        if v == 'PROCESS' : return u'加工/维修'
    if c == 'LOG':
        if v == LOG_TYPE_CREATE : return u'创建'
        if v == LOG_TYPE_UPDATE : return u'更新'
        if v == LOG_TYPE_DEL : return u'删除'
        if v == LOG_TYPE_APPROVE : return u'批准'
        if v == LOG_TYPE_REJECT : return u'拒绝'

    return v
