# -*- coding: utf-8 -*-

from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sys2do.setting import SQLALCHEMY_DATABASE_URI, SQLALCHEMY_DATABASE_POOLSIZE

engine = create_engine( SQLALCHEMY_DATABASE_URI, echo = False,
                        pool_size = SQLALCHEMY_DATABASE_POOLSIZE )
maker = sessionmaker( bind = engine, autoflush = True, autocommit = False )
DBSession = scoped_session( maker )
DeclarativeBase = declarative_base()
metadata = DeclarativeBase.metadata

#===============================================================================
# shortcut
#===============================================================================
db = DBSession
qry = DBSession.query


#===============================================================================
# import sub module
#===============================================================================
from auth import User, Group, Permission
from system import SysDict, SysLog, SysFile
from master import *
from logic import *
