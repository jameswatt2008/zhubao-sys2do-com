var srhResult = [];


function init(){
    $(".priceclz,.qtyclz").change(function(){
        countAmount();
    })
    
    $(".num").numeric();
}

function srhPdtObj(v){
    for(var i=0;i<srhResult.length;i++){
        var t = srhResult[i];
        //if(t.id == id){ return t ;}
        if(t.no == v){ return t;}
    }
    return null;
}



function changeSupplier(){
    var v = $("#supplierID").val();
    
    $("#att").val('');
    $("#tel").val('');
    $("#mobile").val('');
    
    if(!v){ return ;}
    
    var params = {
        'id' : v,
        't' : nowstr()
    }
    $.getJSON('/ajax/supplier',params,function(r){
        if(r.code!=0){
            alert(r.msg);
        }else{
            //alert(r.data.no);
            $("#att").val(r.data.att);
            $("#tel").val(r.data.tel);
            $("#mobile").val(r.data.mobile);
        } 
    })
}


function countAmount(){
    var amount= 0 ;        
    $(".priceclz").each(function(){
        var t = $(this);
        var tr = $(t.parents("tr")[0])
        var q = $(".qtyclz",tr);        
        if(t.val() && q.val()){
            amount += parseFloat(t.val()) * parseInt(q.val());
        }
    });
    $("#amount").val(amount);        
        
}



//###############################################################
//  auto complete for the item input 
//
//###############################################################
$(document).ready(function(){
    
    $('#fieldValue').typeahead({
        source: function (query, process) {
            var params = {
                'f' : $("#field").val(),
                'q' : query,
                't' : nowstr()
            }        
            return $.getJSON("/prh/ajaxSrhPdt", params, function (r) {
                srhResult = r.data;
                var products = [];
                for(var i=0;i<r.data.length;i++){
                    var t = r.data[i];
                    products.push(t.no + '-' + t.name);
                }         
                return process(products);
            });
        },
        updater:function (item) {
            return item;
        }
    });



    $('#fieldValue').keydown(function(event) {
        var t = $(this);
        if (event.which == 13) {
            addProduct();
            event.preventDefault();
            t.val('');            
        }
    }).blur(function(){
        setTimeout('addProduct()', 200)
    });

})


function addProduct(){
    var v = $('#fieldValue').val();
    if(!v){ return ;}
    var no = v.split("-")[0];
    var obj = srhPdtObj(no);
    if(obj){
        //check whether the no is exist
        if(extPdts.indexOf(obj.no) >= 0 ){ return ; }
        else{ extPdts.push(obj.no);}
        var html = '<tr>';
        html += '<td><a href="#" onclick="showPrice('+obj.no+')"><i class="icon-time"></i></a><br /><a href="#" onclick="rmProduct(this)"><i class="icon-remove"></i></a></td>';
        html += '<td><a href="/pdt/view?id='+obj.id+'" target="_blank" class="pdtlink">'+obj.no+'</a></td>';
        html += '<td>'+obj.name+'<input type="hidden" name="product_'+obj.id+'" value="'+obj.id+'"/></td>';
        html += '<td>'+obj.type+'</td>';
        html += '<td>'+obj.pPrice+'<input type="hidden" name="pPrice_'+obj.id+'" value="'+obj.pPrice+'"/></td>';
        html += '<td><input type="text" name="realPrice_'+obj.id+'" value="'+obj.pPrice+'" class="span2 num priceclz"/></td>';
        html += '<td><input type="text" name="qty_'+obj.id+'" value="1" class="span1 num qtyclz"/></td>';
        html += '<td><textarea name="remark_'+obj.id+'"></textarea></td>';
        html += '</tr>';
        $("#pdtlist").append(html);
        init();
        countAmount();
    }
}


function rmProduct(obj){
    var t = $(obj);
    var tr = $(t.parents("tr")[0]);
    var k = $('a',tr).text();
    var lct = extPdts.indexOf(k);
    if(lct >= 0 ){ extPdts.splice(lct,1); } //remove the key in the ext product
    tr.remove();
    
}

