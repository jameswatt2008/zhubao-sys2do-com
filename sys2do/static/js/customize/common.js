function s4() {
    return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
}
function guid() {
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}
function redirect(url) {
    window.location = url;
}

function redirect_alert(msg,url){
    if(confirm(msg)){
        redirect(url);
    }else{
        return false;
    }
}

function refresh(){
    window.location.reload();
}


function nowstr(){
    return Date.parse(new Date())
}

function check_mobile(v){
    var pattern = /^1\d{10}$/;
    return pattern.test(v);
}


function check_number(v){
    var pattern = /^[\d\.]+$/;
    return pattern.test(v); 
}


function selectall(obj,name){
    var t = $(obj);
    if(t.attr('checked')){
        $("input[type='checkbox'][name='"+name+"']").attr('checked','checked');
    }else{
        $("input[type='checkbox'][name='"+name+"']").removeAttr('checked');
    }
}


if(!String.prototype.trim) {
    //code for trim
    String.prototype.trim=function(){return this.replace(/^\s\s*/, '').replace(/\s\s*$/, '');};
 }
 

(function($){
    $.fn.extend({ 
        //pass the options variable to the function
        radioval: function() {
            if(arguments.length < 1){
                return $(this).filter("[checked]").val();
            }else{
                $(this).removeAttr('checked');
                //add   .attr('defaultChecked','true') to fix the IE problem             
                $(this).filter("[value='" + arguments[0] +"']").attr('defaultChecked','true').attr('checked','true'); 
            }
        }
    })
})(jQuery);



function showShopList(){
    var params = {
        
        't' : nowstr()
    }
    $.getJSON('/ajax/getShopList',params,function(r){
        if(r.code!=0){
            
        }else{
            var html = '';
            for(var i = 0;i<r.data.length;i++){
                var t = r.data[i];
                html += '<li><a href="/switch?id='+t.id+'">'+t.name+'</a></li>';
            }
            $("#shoplist").html(html);
        }
    })
}



atmcount = 10
function addAtm(templatecls,markcls,appendto){
    atmcount +=1 ;
    var tmp = $('.'+templatecls).clone();
    tmp.removeClass(templatecls).addClass(markcls);
    
    $("input,textarea,select",tmp).not("[type='button']").each(function(){
        var t = $(this);
        var n = t.attr("name");
        t.attr("name",n.replace("_x","_"+atmcount));
    });
    $(appendto).append(tmp);
    return false;
}

function delAtm(obj,markcls){
    var t = $(obj);
    $(t.parents("." + markcls)[0]).remove();
    return false;
}


function delExtAtm(obj,id,hiddenid,markcls){
    if(!confirm('确定删除该文件吗？')){ return ;}
    var l = extAtm.indexOf(id); 
    if( l > -1){
        extAtm.splice(l,1);
        $("#" + hiddenid).val(extAtm.join("|"));
    }
    var t = $(obj);
    $(t.parents("." + markcls)[0]).remove();
    return true;
}


function delExtImg(obj,id,hiddenid,markcls){
    var l = extImg.indexOf(id); 
    if( l > -1){
        extImg.splice(l,1);
        $("#" + hiddenid).val(extImg.join("|"));
    }
    var t = $(obj);
    $(t.parents("." + markcls)[0]).remove();
    return true;
}



function refreshMaster(eid,mid){
    $.getJSON("/mi",{
        'mid' : mid,
        't' : nowstr()
    },function(r){
        if(r.code!=0){
            alert(r.msg);
        }else{
            var t = $('#'+eid);
            options = "<option value=''></option>";
            for(var i=0;i<r.data.length;i++){
                var o = r.data[i];
                options += "<option value='"+o.id+"'>"+o.val+"</option>"
            }
            t.html(options)
        }
    })
}


function alertError(msg){
    var html = '<div class="alert alert-error">';
    html += '<button type="button" class="close" data-dismiss="alert">&times;</button>';
    html += '<strong>警告!</strong>&nbsp;&nbsp;<ul>';
    for(var i=0;i<msg.length;i++){
        html += '<li>' + msg[i] + '</li>';
    }
    html += '</ul></div>';
    
    $(".msgdiv").html(html);
}

function clearMsg(){
    $(".msgdiv").html('');
}




function provinceChange(obj,city,handler){
    var t = $(obj);
    
    var c = $(city);
    c.empty();
    if(!t.val()){
        return ;
    }
    
    $.getJSON('/ajax/province',
             {
                't' : nowstr(),
                'pid' : $(":selected",t).val()
             },
             function(r){
                 if(r.code!=0){
                     
                 }else{
                     var html = '<option value=""></option>';
                     for(var i=0;i<r.data.length;i++){
                         var tmp = r.data[i];
                         html += '<option value="'+tmp.id+'">'+tmp.name+'</option>';
                     }
                     c.html(html);
                 }
                 
                 if(handler){
                     handler();
                 }
                 
             }
    );
    
}