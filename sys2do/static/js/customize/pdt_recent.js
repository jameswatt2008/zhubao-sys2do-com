function showPrice(no){
    var params = {
        'q' : no,
        't' : nowstr()
    }
    $.getJSON('/ajax/searchPOPrice',params,function(r){
        if(r.code != 0){
            alert(r.msg);
        }else{
            var html = '';
            for(var i=0;i<r.data.length;i++){
                var d = r.data[i];
                html += '<tr>';
                html += '<td>'+d.time+'</td>';
                html += '<td><a href="/prh/view?id='+d.pid+'" target="_blank">'+d.orderno+'</a></td>';
                html += '<td>'+d.product_no+'</td>';
                html += '<td>'+d.product_name+'</td>';
                html += '<td>'+d.price+'</td>';
                html += '<td>'+d.creator+'</td>';
                html += '</tr>';
            }
            $("#data_list").html(html);
            $("#appModal").modal('show');            
        }
    })
}
