# -*- coding: utf-8 -*-
import os, datetime, uuid, logging

DEBUG = True

# upload setting
UPLOAD_FOLDER_PREFIX = os.path.dirname( __file__ )
UPLOAD_FOLDER = os.path.join( "static", "upload" )
UPLOAD_FOLDER_URL = "/static/upload"
ALLOWED_EXTENSIONS = set( ['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif', 'doc'] )

TMP_FOLDER = os.path.join( os.path.dirname( __file__ ), "static", "tmp" )
TEMPLATE_FOLDER = os.path.join( os.path.dirname( __file__ ), "static", "template" )

NO_IMAGE_URL = '/static/img/system/noimage.jpg'

# session setting

SECRET_KEY = str( uuid.uuid4() )
USE_X_SENDFILE = False
SERVER_NAME = None
MAX_CONTENT_LENGTH = None
TESTING = False
PERMANENT_SESSION_LIFETIME = datetime.timedelta( days = 31 )
SESSION_COOKIE_NAME = 'session'

# Beaker session setting
BEAKER_SESSION = {
                  'session.type': 'file',
                  'session.data_dir': os.path.join( os.path.dirname( __file__ ), "session", "data" ),
                  'session.lock_dir': os.path.join( os.path.dirname( __file__ ), "session", "lock" ),
                  }

# config for logging
LOGGING_FILE = True
LOGGING_FILE_PATH = os.path.join( os.path.dirname( __file__ ), "..", "system_log.txt" )
LOGGING_LEVEL = logging.DEBUG    # logging.DEBUG
LOGGING_MSG_FORMAT = '%(asctime)s %(levelname)s:\n%(message)s'



# database setting
# sqlite
# SQLALCHEMY_DATABASE_URI = 'sqlite:///%s' % (os.path.join(os.path.dirname(__file__), ".." , "new.db"))
# mysql
# SQLALCHEMY_DATABASE_URI = 'mysql://username:password@hostname:port/databasename'
# postgresql
SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:admin@192.168.21.157:5433/kk'
SQLALCHEMY_DATABASE_POOLSIZE = 20

# setting for paginate ,every page is 20 recore
PAGINATE_PER_PAGE = 10

# email server ip
EMAIL_SERVER_IP = "192.168.42.14"
EMAIL_SEND_FROM = "r-track@r-pac.com.hk"
