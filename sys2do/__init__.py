# -*- coding: utf-8 -*-
import logging
import logging.handlers
from flask import Flask, Module
from flask_beaker import BeakerSession
from flaskext.babel import Babel
from flask.helpers import url_for

__all__ = ["app"]

app = Flask( __name__, static_url_path = '/static' )
app.config.from_object( "sys2do.setting" )
beaker = BeakerSession( app )    # set the beader session plugin
babel = Babel( app )    # set the babel i18n plugin

# app.debug = True
#===============================================================================
# log to file if it's LIVE environment
#===============================================================================
if not app.debug:
    fileHandler = logging.handlers.TimedRotatingFileHandler( app.config['LOGGING_FILE_PATH'], when = 'h', encoding = None, delay = False )
    fileHandler.setLevel( app.config['LOGGING_LEVEL'] )
    fileHandler.setFormatter( logging.Formatter( app.config['LOGGING_MSG_FORMAT'] ) )
    app.logger.addHandler( fileHandler )

#===============================================================================
# root.py
#===============================================================================
import views.root
app.register_blueprint( views.root.bpRoot )


import views.auth
app.register_blueprint( views.auth.bpAuth, url_prefix = '/auth' )

import views.mas
app.register_blueprint( views.mas.bpMas, url_prefix = '/m' )

import views.inventorynote
app.register_blueprint( views.inventorynote.bpIvtnt, url_prefix = '/ivtnt' )

import views.master.shop
app.register_blueprint( views.master.shop.bpShop, url_prefix = '/sp' )

import views.inventory
app.register_blueprint( views.inventory.bpIvt, url_prefix = '/ivt' )

import views.master.product
app.register_blueprint( views.master.product.bpPdt, url_prefix = '/pdt' )

import views.master.supplier
app.register_blueprint( views.master.supplier.bpSpl, url_prefix = '/spl' )

import views.master.code
app.register_blueprint( views.master.code.bpCode, url_prefix = '/code' )

import views.master.currency
app.register_blueprint( views.master.currency.bpCry, url_prefix = '/cry' )

import views.master.paytype
app.register_blueprint( views.master.paytype.bpPtp, url_prefix = '/ptp' )

import views.member
app.register_blueprint( views.member.bpMem, url_prefix = '/mem' )

import views.transfer
app.register_blueprint( views.transfer.bpTns, url_prefix = '/tns' )

import views.sale
app.register_blueprint( views.sale.bpSale, url_prefix = '/sale' )

import views.purchase
app.register_blueprint( views.purchase.bpPrh, url_prefix = '/prh' )

import views.returngoods
app.register_blueprint( views.returngoods.bpRg, url_prefix = '/rg' )

import views.report
app.register_blueprint( views.report.bpRpt, url_prefix = '/rpt' )


import views.finance
app.register_blueprint( views.finance.bpFin, url_prefix = '/fin' )

import views.ajax
app.register_blueprint( views.ajax.bpAjax, url_prefix = '/ajax' )

import views.trace
app.register_blueprint( views.trace.bpTrc, url_prefix = '/trc' )

#===============================================================================
# sys.py
#===============================================================================
import views.sys
for error_code in [403, 404] : app.error_handler_spec[None][error_code] = views.sys.error_page( error_code )
app.register_blueprint( views.sys.bpSys, url_prefix = '/sys' )
#===============================================================================
# import the customize filter and testing
#===============================================================================
import util.filters as filters
for f in filters.__all__ : app.jinja_env.filters[f] = getattr( filters, f )

import util.tests as tests
for t in tests.__all__ : app.jinja_env.tests[t] = getattr( tests, t )

#===============================================================================
# import the global vars into the template
#===============================================================================
import constant
for c in dir( constant ) :
    if c.isupper() : app.jinja_env.globals[c] = getattr( constant, c )


import util.tpl_func as func
for f in func.__all__ : app.jinja_env.globals[f] = getattr( func, f )

