# -*- coding: utf-8 -*-
'''
Created on 2013-4-11

@author: cl.lam
'''

import traceback
from flask import current_app as app
from flask import g, render_template, flash, session, redirect, url_for, request
from flask.blueprints import Blueprint
from sqlalchemy.sql.expression import and_, not_


from sys2do.views import BasicView
from sys2do.util.decorator import templated, loginRequired, activetab
from sys2do.model import db, qry, Group, Shop, ShopProfile, InventoryLocation

from sys2do.util.common import _g, _gld
from sys2do.util.logic_helper import getCurrentShopID
from sys2do.constant import MSG_SAVE_SUCC, TAB_BASIC, TAB_FLAG
from sys2do.model.logic import InventoryProduct, InventoryNote
from sys2do.model.system import SysDict


__all__ = ['bpIvt']

bpCode = Blueprint( 'bpCode', __name__ )
@bpCode.before_request
def addActivetab():    session[TAB_FLAG] = TAB_BASIC


class CodeView( BasicView ):

    template_folder = "mas.code"

    @templated( "index.html" )
    @loginRequired
    def index( self ):
        form = SrhForm( request.form )
        form.url = url_for( '.view' )

        cds = [ SysDict.active == 0, SysDict.type == 'OBJECT_PREFIX' ]
        if form.name.data : cds.append( SysDict.name.like( '%%%s%%' % form.name.data ) )
        if form.value.data : cds.append( SysDict.value.like( '%%%s%%' % form.value.data ) )

        result = qry( SysDict ).filter( and_( *cds ) ).order_by( SysDict.name )
        return {'result' : result , 'form' : form}


    @templated( "add.html" )
    @loginRequired
    def add( self ):
        pass



    @templated( "update.html" )
    @loginRequired
    def update( self ):
        pass


bpCode.add_url_rule( '/', view_func = CodeView.as_view( 'view' ), defaults = {'action':'index'} )
bpCode.add_url_rule( '/<action>', view_func = CodeView.as_view( 'view' ) )

#===============================================================================
# form class
#===============================================================================

from wtforms import Form, TextField


class SrhForm( Form ):
    name = TextField( u'编码名称', )
    value = TextField( u'编码值', )

