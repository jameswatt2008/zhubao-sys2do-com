# -*- coding: utf-8 -*-

try:
    from setuptools import setup, find_packages
except ImportError:
    from ez_setup import use_setuptools
    use_setuptools()
    from setuptools import setup, find_packages

setup( 
    name = 'zhubao',
    version = '0.1',
    description = u'比较简单的珠宝进销存系统，包括销售，采购，调拨，仓存，分店加盟，自营模式，采用Flask,Postgresql，Bootstrap来搭建，适合中小型珠宝公司。',
    author = 'CL.Lam',
    author_email = 'lamciuloeng@gmail.com',
    # url='',
    install_requires = [],
    setup_requires = ["PasteScript >= 1.7", "flask >= 0.9", "flask_beaker >= 0.2",
                      "flaskext.babel >= 0.7", "sqlalchemy >= 0.8", "PIL",
                      "elaphe >= 0.5", "wtforms >= 1.0"],
    paster_plugins = ['Framework'],
    packages = find_packages( exclude = ['ez_setup'] ),
    include_package_data = True,
    test_suite = 'nose.collector',
    tests_require = [],
    package_data = {'sys2do': ['templates/*/*', 'public/*/*']},


    entry_points = """
    """,
 )
