# -*- coding: utf-8 -*-

import sys
reload( sys )
sys.setdefaultencoding( 'utf8' )

# turn off the log for every request response to
from werkzeug.serving import WSGIRequestHandler
WSGIRequestHandler.log = lambda self, type, message, *args: None



from sys2do import app


if __name__ == '__main__':
    app.run( host = 'localhost', port = 5000 )
